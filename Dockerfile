# Demo that pyOpenSSL 0.13.1 is not affected by CVE-2018-1000807
# https://github.com/pyca/pyopenssl/pull/723
#
# podman build -t localhost/pyopenssl .
# podman run --rm -ti -v .:/pyopenssl:Z localhost/pyopenssl
FROM centos:7
RUN yum -y install python-devel gcc openssl-devel python-setuptools git openssl
VOLUME ["/pyopenssl"]
WORKDIR /pyopenssl
CMD ["sh", "-c", "python setup.py build_ext -i && python -m unittest discover -v"]
